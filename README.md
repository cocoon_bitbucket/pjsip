pjsip
=====

docker image to build pjsip projects




build and install pjsip 2.1.0

create container

```
docker run -ti -v /tmp:/tmp cocoon/pjsip
```

install pjsua

```
cd /projects/pjproject-2.1.0/
python python_pjsip_install.sh

```
package is available on host at /tmp/pjsua-*.tar.gz




test it

```
cd /projects/pjproject-2.1.0/tests/pjsua
python run.py -n mod_run.py scripts-run 100_simple.py
```