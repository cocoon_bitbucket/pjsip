#
#  patchs
#

# patch config_site for ims compatibility and cancel RTCP advertise
cp ./templates/config_site.h ./pjlib/include/pj/config_site.h

# patch LDFLAGS for ims compatibility
cp ./templates/user.mak  ./user.mak

# patch pjsua_call.c for refer bug
cp ./template/pjsua_call.c ./pjsip/src/pjsua-lib/pjsua_call.c


#
# make pjlibs
#
# configure
./configure CFLAGS='-fPIC'


# make dep
make dep 

# make milenage ( for ims aka)
cd /third_party/build/milenage/ && make

# make
make all

#
# make python pjsua
#
cd ./pjsip-apps/src/python
# install python pjsua
python setup.py install

# test pjsua
cd ./tests/pjsua && python run.py -n mod_run.py scripts-run/100_simple.py 

# make python pjsua binary distribution package
python setup.py bdist

#- name: copy pjsua dist to packages
#  command: cp {{pjproject_dir}}/pjsip-apps/src/python/dist/pjsua-{{pjsua_version}}.linux-x86_64.tar.gz /vagrant/packages/
