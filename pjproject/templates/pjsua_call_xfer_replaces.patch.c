/*
 * Initiate attended call transfer to the specified address.
 *
 * LT: adapted to escape @ in Replaces url
 */
PJ_DEF(pj_status_t) pjsua_call_xfer_replaces( pjsua_call_id call_id, 
					      pjsua_call_id dest_call_id,
					      unsigned options,
					      const pjsua_msg_data *msg_data)
{
    pjsua_call *dest_call;
    pjsip_dialog *dest_dlg;
    char str_dest_buf[PJSIP_MAX_URL_SIZE*2];
    pj_str_t str_dest;
    int len;
    pjsip_uri *uri;
    pj_status_t status;

    /* LT adds for escape @ in call_id */
    int call_id_max_len = 64;
    char str_escaped_call_id[ call_id_max_len ];

    char *dst= str_escaped_call_id ;
    char *dst_end = dst + call_id_max_len ;

    int  len_escaped_call_id= 0;
    const char *src ;
    const char *src_end ;


    

    PJ_ASSERT_RETURN(call_id>=0 && call_id<(int)pjsua_var.ua_cfg.max_calls,
		     PJ_EINVAL);
    PJ_ASSERT_RETURN(dest_call_id>=0 && 
		      dest_call_id<(int)pjsua_var.ua_cfg.max_calls,
		     PJ_EINVAL);
    
    PJ_LOG(4,(THIS_FILE, "Transfering call %d replacing with call %d",
			 call_id, dest_call_id));
    pj_log_push_indent();

    status = acquire_call("pjsua_call_xfer_replaces()", dest_call_id, 
			  &dest_call, &dest_dlg);
    if (status != PJ_SUCCESS) {
	pj_log_pop_indent();
	return status;
    }
        
    /* 
     * Create REFER destination URI with Replaces field.
     */

    /* Make sure we have sufficient buffer's length */
    PJ_ASSERT_ON_FAIL(dest_dlg->remote.info_str.slen +
		      dest_dlg->call_id->id.slen +
		      dest_dlg->remote.info->tag.slen +
		      dest_dlg->local.info->tag.slen + 32 
		      < (long)sizeof(str_dest_buf),
		      { status=PJSIP_EURITOOLONG; goto on_error; });

    /* Print URI */
    str_dest_buf[0] = '<';
    str_dest.slen = 1;

    uri = (pjsip_uri*) pjsip_uri_get_uri(dest_dlg->remote.info->uri);
    len = pjsip_uri_print(PJSIP_URI_IN_REQ_URI, uri, 
		          str_dest_buf+1, sizeof(str_dest_buf)-1);
    if (len < 0) {
	status = PJSIP_EURITOOLONG;
	goto on_error;
    }

    str_dest.slen += len;


    /* LT: escape call_id  @ -> %40  */
    src= dest_dlg->call_id->id.ptr ;
    src_end = src + (int)dest_dlg->call_id->id.slen ;

    while (src != src_end && dst != dst_end) {
	    if ( *src != '@') {
	        *dst++ = *src++;
	        ++ len_escaped_call_id;

	    } else {
            if (dst < dst_end-2) {
                *dst++ = '%';
                *dst++ = '4';
                *dst++ = '0';
                ++src;
                len_escaped_call_id += 3;
            } else {
                break;
            }
        }
    }

    /* Build the URI */
    len = pj_ansi_snprintf(str_dest_buf + str_dest.slen,
			   sizeof(str_dest_buf) - str_dest.slen,
			   "?%s"
			   "Replaces=%.*s"
			   "%%3Bto-tag%%3D%.*s"
			   "%%3Bfrom-tag%%3D%.*s>",
			   ((options&PJSUA_XFER_NO_REQUIRE_REPLACES) ?
			    "" : "Require=replaces&"),

			   len_escaped_call_id,
			   str_escaped_call_id,

			   (int)dest_dlg->remote.info->tag.slen,
			   dest_dlg->remote.info->tag.ptr,
			   (int)dest_dlg->local.info->tag.slen,
			   dest_dlg->local.info->tag.ptr);

    PJ_ASSERT_ON_FAIL(len > 0 && len <= (int)sizeof(str_dest_buf)-str_dest.slen,
		      { status=PJSIP_EURITOOLONG; goto on_error; });
    
    str_dest.ptr = str_dest_buf;
    str_dest.slen += len;

    pjsip_dlg_dec_lock(dest_dlg);
    
    status = pjsua_call_xfer(call_id, &str_dest, msg_data);

    pj_log_pop_indent();
    return status;

on_error:
    if (dest_dlg) pjsip_dlg_dec_lock(dest_dlg);
    pj_log_pop_indent();
    return status;
}
