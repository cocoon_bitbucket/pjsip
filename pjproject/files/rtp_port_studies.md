
rtp_port study
==============


cfg->rtp_cfg.port


in pjsua_app_config.c

in function:

/* Parse arguments. */
static pj_status_t parse_args(int argc, char *argv[],
			      pj_str_t *uri_to_call)
{

	case OPT_RTP_PORT:
	    cfg->rtp_cfg.port = my_atoi(pj_optarg);
	    if (cfg->rtp_cfg.port == 0) {
		enum { START_PORT=4000 };
		unsigned range;

		range = (65535-START_PORT-PJSUA_MAX_CALLS*2);
		cfg->rtp_cfg.port = START_PORT +
				    ((pj_rand() % range) & 0xFFFE);
	    }

	    if (cfg->rtp_cfg.port < 1 || cfg->rtp_cfg.port > 65535) {
		PJ_LOG(1,(THIS_FILE,
			  "Error: rtp-port argument value "
			  "(expecting 1-65535"));
		return -1;
	    }
	    break;



in pjsua_appcommon.h

/* Pjsua application data */
typedef struct pjsua_app_config
{
    pjsua_config	    cfg;
    pjsua_logging_config    log_cfg;
    pjsua_media_config	    media_cfg;
    pj_bool_t		    no_refersub;
    pj_bool_t		    ipv6;
    pj_bool_t		    enable_qos;
    pj_bool_t		    no_tcp;
    pj_bool_t		    no_udp;
    pj_bool_t		    use_tls;
    pjsua_transport_config  udp_cfg;

    pjsua_transport_config  rtp_cfg;

    pjsip_redirect_op	    redir_op;


in pjsua_app_cli.c

/* Add account */
static pj_status_t cmd_add_account(pj_cli_cmd_val *cval)
{
    pjsua_acc_config acc_cfg;
    pj_status_t status;

    pjsua_acc_config_default(&acc_cfg);
    acc_cfg.id = cval->argv[1];
    acc_cfg.reg_uri = cval->argv[2];
    acc_cfg.cred_count = 1;
    acc_cfg.cred_info[0].scheme = pj_str("Digest");
    acc_cfg.cred_info[0].realm = cval->argv[3];
    acc_cfg.cred_info[0].username = cval->argv[4];
    acc_cfg.cred_info[0].data_type = 0;
    acc_cfg.cred_info[0].data = cval->argv[5];

    acc_cfg.rtp_cfg = app_config.rtp_cfg;
    app_config_init_video(&acc_cfg);






Searching 91 files for "rtp_cfg"

/Users/cocoon/Documents/hgclones/pjproject/pjsip-apps/src/pjsua/pjsua_app.c:
 1605  
 1606  	    app_config_init_video(&acc_cfg);
 1607: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1608  	    pjsua_acc_modify(aid, &acc_cfg);
 1609  	}
 ....
 1649  
 1650  	    app_config_init_video(&acc_cfg);
 1651: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1652  	    acc_cfg.ipv6_media_use = PJSUA_IPV6_ENABLED;
 1653  	    pjsua_acc_modify(aid, &acc_cfg);
 ....
 1687  
 1688  	    app_config_init_video(&acc_cfg);
 1689: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1690  	    pjsua_acc_modify(aid, &acc_cfg);
 1691  	}
 ....
 1717  
 1718  	    app_config_init_video(&acc_cfg);
 1719: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1720  	    acc_cfg.ipv6_media_use = PJSUA_IPV6_ENABLED;
 1721  	    pjsua_acc_modify(aid, &acc_cfg);
 ....
 1756  
 1757  	    app_config_init_video(&acc_cfg);
 1758: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1759  	    pjsua_acc_modify(acc_id, &acc_cfg);
 1760  	}
 ....
 1785  
 1786  	    app_config_init_video(&acc_cfg);
 1787: 	    acc_cfg.rtp_cfg = app_config.rtp_cfg;
 1788  	    acc_cfg.ipv6_media_use = PJSUA_IPV6_ENABLED;
 1789  	    pjsua_acc_modify(aid, &acc_cfg);
 ....
 1805      /* Add accounts */
 1806      for (i=0; i<app_config.acc_cnt; ++i) {
 1807: 	app_config.acc_cfg[i].rtp_cfg = app_config.rtp_cfg;
 1808  	app_config.acc_cfg[i].reg_retry_interval = 300;
 1809  	app_config.acc_cfg[i].reg_first_retry_interval = 60;

/Users/cocoon/Documents/hgclones/pjproject/pjsip-apps/src/pjsua/pjsua_app_cli.c:
  774      acc_cfg.cred_info[0].data = cval->argv[5];
  775  
  776:     acc_cfg.rtp_cfg = app_config.rtp_cfg;
  777      app_config_init_video(&acc_cfg);
  778  

/Users/cocoon/Documents/hgclones/pjproject/pjsip-apps/src/pjsua/pjsua_app_common.h:
   83      pj_bool_t		    use_tls;
   84      pjsua_transport_config  udp_cfg;
   85:     pjsua_transport_config  rtp_cfg;
   86      pjsip_redirect_op	    redir_op;
   87  

/Users/cocoon/Documents/hgclones/pjproject/pjsip-apps/src/pjsua/pjsua_app_config.c:
  643  	case OPT_IP_ADDR: /* ip-addr */
  644  	    cfg->udp_cfg.public_addr = pj_str(pj_optarg);
  645: 	    cfg->rtp_cfg.public_addr = pj_str(pj_optarg);
  646  	    break;
  647  
  648  	case OPT_BOUND_ADDR: /* bound-addr */
  649  	    cfg->udp_cfg.bound_addr = pj_str(pj_optarg);
  650: 	    cfg->rtp_cfg.bound_addr = pj_str(pj_optarg);
  651  	    break;
  652  
  ...
 1032  
 1033  	case OPT_RTP_PORT:
 1034: 	    cfg->rtp_cfg.port = my_atoi(pj_optarg);
 1035: 	    if (cfg->rtp_cfg.port == 0) {
 1036  		enum { START_PORT=4000 };
 1037  		unsigned range;
 1038  
 1039  		range = (65535-START_PORT-PJSUA_MAX_CALLS*2);
 1040: 		cfg->rtp_cfg.port = START_PORT +
 1041  				    ((pj_rand() % range) & 0xFFFE);
 1042  	    }
 1043  
 1044: 	    if (cfg->rtp_cfg.port < 1 || cfg->rtp_cfg.port > 65535) {
 1045  		PJ_LOG(1,(THIS_FILE,
 1046  			  "Error: rtp-port argument value "
 ....
 1283  	    cfg->enable_qos = PJ_TRUE;
 1284  	    /* Set RTP traffic type to Voice */
 1285: 	    cfg->rtp_cfg.qos_type = PJ_QOS_TYPE_VOICE;
 1286  	    /* Directly apply DSCP value to SIP traffic. Say lets
 1287  	     * set it to CS3 (DSCP 011000). Note that this will not
 ....
 1434      pjsua_transport_config_default(&cfg->udp_cfg);
 1435      cfg->udp_cfg.port = 5060;
 1436:     pjsua_transport_config_default(&cfg->rtp_cfg);
 1437:     cfg->rtp_cfg.port = 4000;
 1438      cfg->redir_op = PJSIP_REDIRECT_ACCEPT_REPLACE;
 1439      cfg->duration = PJSUA_APP_NO_LIMIT_DURATION;
 ....
 2084      /* Start RTP port. */
 2085      pj_ansi_sprintf(line, "--rtp-port %d\n",
 2086: 		    config->rtp_cfg.port);
 2087      pj_strcat2(&cfg, line);
 2088  

/Users/cocoon/Documents/hgclones/pjproject/pjsip-apps/src/pjsua/pjsua_app_legacy.c:
  885  }
  886  
  887: static void ui_add_account(pjsua_transport_config *rtp_cfg)
  888  {
  889      char id[80], registrar[80], realm[80], uname[80], passwd[30];
  ...
  912      acc_cfg.cred_info[0].data = pj_str(passwd);
  913  
  914:     acc_cfg.rtp_cfg = *rtp_cfg;
  915      app_config_init_video(&acc_cfg);
  916  
  ...
 1763  		ui_add_buddy();
 1764  	    } else if (menuin[1] == 'a') {
 1765: 		ui_add_account(&app_config.rtp_cfg);
 1766  	    } else {
 1767  		printf("Invalid input %s\n", menuin);

<untitled 611>:
    1: cfg->rtp_cfg.port
    2  
    3  
    .
   18      pjsua_transport_config  udp_cfg;
   19  
   20:     pjsua_transport_config  rtp_cfg;
   21  
   22      pjsip_redirect_op	    redir_op;
   ..
   41      acc_cfg.cred_info[0].data = cval->argv[5];
   42  
   43:     acc_cfg.rtp_cfg = app_config.rtp_cfg;
   44      app_config_init_video(&acc_cfg);
   45  

36 matches across 6 files
