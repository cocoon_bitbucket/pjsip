./configure --host=i386-unknown-linux-gnu
make dep
make
# install python pjsua lib
cd pjsip-apps/src/python
CFLAGS=-m32 python setup.py install
# make dist
CFLAGS=-m32 python setup.py bdist -p i386
cp dist/pjsua-*.i386.tar.gz /tmp/
# test it 
cd /projects/pjproject-2.1.0/tests/pjsua
python run.py -n mod_run.py scripts-run/100_simple.py

