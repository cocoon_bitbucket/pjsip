./configure CFLAGS='-fPIC'
make dep
make
# install python pjsua lib
cd pjsip-apps/src/python

# make dist
python setup.py bdist

# install pjsua
python setup.py install


# test it 
cd /projects/pjproject-2.1/tests/pjsua
python run.py -n mod_run.py scripts-run/100_simple.py

# build pjsua2 (swig)
#cd pjsip-apps/src/swig/python
#python setup.py bdist

# copy dist to packages
cp /projects/pjproject-2.1/pjsip-apps/src/python/dist/pjsua-*.tar.gz /packages/